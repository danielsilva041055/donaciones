// importamos el modulo 'pg' para manejar PostgreSQL desde el servidor de Node
const { Pool } = require('pg');


const path = require('path');

//establecemos conexion con el servidor del proyecto
const pool = new Pool({
    host: 'localhost',
    user: 'postgres',
    password: '1234', //ponga la clave de usuario postgres de su pc
    database: 'gabriel', //nombre que le hayan puesto a la bd del proyecto en su pc
    port: '5432'
})

//----------------------------------------------------------------------------------------------------------------------------------



// aqui se definen las funciones que seran invocadas en su ruta correspondiente

    //funcion que optiene todos los usurarios regitrados 
        const getUsers = async (req, res) => {
            const response_admins = await pool.query('SELECT * FROM public.administrador');
            const response_agentes = await pool.query('SELECT * FROM public.agente');
            const response_lcs = await pool.query('SELECT * FROM public.lider_campana');
            let res_front = [].concat(response_admins.rows, response_agentes.rows, response_lcs.rows);
            res.status(200).json(res_front);
        }

    //funcion que busca un usuario en particular 
        const getUser = async (req, res) => {
            const nombre = req.params.nombre;
            const contrasena = req.params.pass;
            let Admin = await pool.query('SELECT * FROM public.administrador WHERE nombre = $1',[nombre]);
            let Agente = await pool.query('SELECT * FROM public.agente WHERE nombre = $1',[nombre]);
            let Lc = await pool.query('SELECT * FROM public.lider_campana WHERE nombre = $1',[nombre]);
            if (Admin.rowCount == 1){
                if (Admin.rows[0].contrasena == contrasena){
                    res.send('el usuario administrador '+ nombre +' es correcto')
                }else{
                    res.send('contrasena incorrecta')
                }
            }else if (Agente.rowCount == 1){
                if (Agente.rows[0].contrasena = contrasena){
                    res.send('el usuario administrador '+ nombre +' es correcto')
                }else{
                    res.send('contrasena incorrecta')
                }
            }else if (Lc.rowCount == 1){
                if (Lc.rows[0].contrasena = contrasena){
                    res.send('el usuario lider de campana '+ nombre +' es correcto')
                }else{
                    res.send('contrasena incorrecta')
                }
            }else{
                res.send('el usuario no existe')
            }
        }
    
    //funcion que optiene todos las campanas regitrados 
        const getCampanas = async (req, res) => {
            const response = await pool.query('SELECT * FROM public.campana');
            res.status(200).json(response.rows);
        }

    //funcion que optiene todos los productos regitrados 
        const getProductos = async (req, res) => {
            const response = await pool.query('SELECT * FROM public.producto');
            res.status(200).json(response.rows);
        }
    
    //funcion que optiene todos las donaciones y ordenes regitradas 
        const getDonaciones = async (req, res) => {
            const responseD = await pool.query('SELECT * FROM public.donacion');
            const responseO = await pool.query('SELECT * FROM public.orden');
            let res_front = [].concat(responseD.rows, responseO.rows);
            res.status(200).json(res_front);
        }


    //funcion que inserta un usuario de nivel administrador en la bd si este no existe 
        const setUserAdmin = async (req, res) => {
            const { contrasena, correo, nombre } = req.body;
            let existe = await pool.query(
                'SELECT * FROM public.administrador WHERE nombre = $1',
                [nombre]
            );
            if (existe.rowCount == 0){
                const response = await pool.query(
                    'INSERT INTO public.administrador(contrasena, correo, nombre) VALUES ($1, $2, $3)',
                    [contrasena, correo, nombre]
                    );
                console.log(response);
                res.send('el administrador '+ nombre +' ha sido creado')
            }else{
                res.send('the username '+ nombre +' is taken xD')
            }
        };

    //funcion que inserta un usuario de nivel agente en la bd si este no existe
        const setUserAgente = async (req, res) => {
            const { contrasena, correo, nombre } = req.body;
            let existe = await pool.query(
                'SELECT * FROM public.agente WHERE nombre = $1',
                [nombre]
            );
            if (existe.rowCount == 0){
                const response = await pool.query(
                    'INSERT INTO public.agente(contrasena, correo, nombre) VALUES ($1, $2, $3)',
                    [contrasena, correo, nombre]
                    );
                console.log(response);
                res.send('el agente '+ nombre +' ha sido creado')
            }else{
                res.send('the username '+ nombre +' is taken xD')
            }
        };

    //funcion que inserta un usuario de nivel lider de campana en la bd si este no existe
        const setUserLc = async (req, res) => {
            const { contrasena, correo, nombre } = req.body;
            let existe = await pool.query(
                'SELECT * FROM public.lider_campana WHERE nombre = $1',
                [nombre]
            );
            console.log(existe);
            if (existe.rowCount == 0){
                const response = await pool.query(
                    'INSERT INTO public.lider_campana(contrasena, correo, nombre) VALUES ($1, $2, $3)',
                    [contrasena, correo, nombre]
                    );
                console.log(response);
                res.send('el lider de campana '+ nombre +' ha sido creado')
            }else{
                res.send('the username '+ nombre +' is taken xD')
            }
        };

    //funcion que inserta una campana en la bd 
        const setCampana = async (req, res) => {
            const {monto_obj, nombre } = req.body;
            let response = await pool.query(
                'INSERT INTO public.campana(monto_objetivo, total_recaudado, porcentaje_visitas, nombre) VALUES ($1, 0, 0, $2);',
                [monto_obj, nombre]
            );
            console.log(response);
            res.send('campana '+ nombre +' creada')
        };

    //funcion que inserta una donacion en la bd si su camapana existe
        const setDonacion = async (req, res) => {
            const {id_campana, monto} = req.body;
            let existe = await pool.query(
                'SELECT * FROM public.campana WHERE id = $1', [id_campana]
            );
            if (existe.rowCount == 1){
                let response = await pool.query(
                    'INSERT INTO public.donacion(id_campana, monto) VALUES ($1, $2);',
                    [id_campana, monto]
                );
                // let codigo = pool.query('SELECT * FROM public.donacion ORDER BY id DESC LIMIT 2');
                // console.log(codigo);
                res.send('donacion creada, tu codigo es '); // no se como conseguir el id de la donacion
            }else{
                res.send('la campana en la que queire donar no existe');
            }
        };

    //funcion que inserta un producto en la bd si su campana existe
        const setProducto = async (req, res) => {
            const {nombre, precio, id_campana} = req.body;
            let existe = await pool.query(
                'SELECT * FROM public.campana WHERE id = $1', [id_campana]
            );
            if (existe.rowCount == 1){
                let response = await pool.query(
                    'INSERT INTO public.producto(nombre, precio, id_campana) VALUES ($1, $2, $3);',
                    [nombre, precio, id_campana]
                );
                res.send('producto creado'); 
            }else{
                res.send('la campana a la que el producto pertenece no existe');
            }
        };

    //funcion que inserta una orden en la bd s
        const setOrden = async (req, res) => {
            const {id_campana, monto, id_producto} = req.body;
            let existe = await pool.query(
                'SELECT * FROM public.campana WHERE id = $1', [id_campana]
            );
            if (existe.rowCount == 1){
                existe = await pool.query(
                    'SELECT * FROM public.producto WHERE id = $1', [id_producto]
                );
                if (existe.rowCount == 1){
                    let response = await pool.query(
                        'INSERT INTO public.orden(id_campana, monto, id_producto) VALUES ($1, $2, $3);',
                        [id_campana, monto, id_producto]
                    );
                    console.log(response);
                    res.send('orden creada');
                }else{
                    res.send('el producto que quiere adquirir al donar no existe');
                }
            }else{
                res.send('la campana en la que queire donar no existe');
            }
        };

    //funcion que elimina un usuario de nivel administrador en la bd si este existe y siempre y cuando quede al menos un admin
        const deleteUserAdmin = async (req, res ) => {
            const nombre = req.params.nombre;
            let numero_de_admins = await pool.query('SELECT * FROM public.administrador');
            let n_admins = numero_de_admins.rowCount;
            let aux = null;
            console.log(numero_de_admins);
            if (n_admins > 1){
                let existe = await pool.query(
                    'SELECT * FROM public.administrador WHERE nombre = $1',
                    [nombre]
                );
                existe = existe.rowCount;
                if (existe == 1){
                    const response = await pool.query(
                        'DELETE FROM public.administrador WHERE nombre = $1',
                        [nombre]
                    );
                    aux = response;
                    console.log(aux);
                    res.send('administrador '+ nombre +' ha sido eliminado :c');
                }else{
                    res.send('el administrador '+ nombre +' que quiere eliminar no existe');
                }
            }else{
                res.send('debe haber al menos un administrador');
            }
        };

    //funcion que elimina un usuario de nivel agente en la bd si este existe
        const deleteUserAgente = async (req, res ) => {
            const nombre = req.params.nombre;
            let aux = null;
            let existe = await pool.query(
                'SELECT * FROM public.agente WHERE nombre = $1',
                [nombre]
            );
            if (existe.rowCount == 1){
                const response = await pool.query(
                    'DELETE FROM public.agente WHERE nombre = $1',
                    [nombre]
                );
                aux = response;
                console.log(aux);
                res.send('agente '+ nombre +' ha sido eliminado :c');
            }else{
                res.send('el agente '+ nombre +' que quiere eliminar no existe');
            }
        };

    //funcion que elimina un usuario de nivel lider de campana en la bd si este existe 
        const deleteUserLc = async (req, res ) => {
            const nombre = req.params.nombre;
            let aux = null;
            let existe = await pool.query(
                'SELECT * FROM public.lider_campana WHERE nombre = $1',
                [nombre]
            );
            if (existe.rowCount == 1){
                const response = await pool.query(
                    'DELETE FROM public.lider_campana WHERE nombre = $1',
                    [nombre]
                );
                aux = response;
                console.log(aux);
                res.send('lider de campana '+ nombre +' ha sido eliminado :c');
            }else{
                res.send('el lider de campana '+ nombre +' que quiere eliminar no existe');
            }
        };

    //funcion que elimina una campana en la bd si este existe 
        const deleteCampana = async (req, res ) => {
            const id = req.params.id;
            let aux = null;
            let existe = await pool.query(
                'SELECT * FROM public.campana WHERE id = $1',
                [id]
            );
            if (existe.rowCount == 1){
                const response = await pool.query(
                    'DELETE FROM public.campana WHERE id = $1',
                    [id]
                );
                aux = response;
                console.log(aux);
                res.send('la campana ha sido eliminada :c');
            }else{
                res.send('la campana que quiere eliminar no existe');
            }
        };

    //funcion que elimina un producto en la bd si este existe 
        const deleteProducto = async (req, res ) => {
            const id = req.params.id;
            let aux = null;
            let existe = await pool.query(
                'SELECT * FROM public.producto WHERE id = $1',
                [id]
            );
            if (existe.rowCount == 1){
                const response = await pool.query(
                    'DELETE FROM public.producto WHERE id = $1',
                    [id]
                );
                aux = response;
                console.log(aux);
                res.send('el producto ha sido eliminado :c');
            }else{
                res.send('el producto que quiere eliminar no existe');
            }
        };
        
    //funcion que renderiza el html de la entrada
        const renderizarEntrada = (req, res) => {
            console.log(__dirname);
            res.sendFile(path.resolve(__dirname, 'front/entrada.html'));
        }
        
    //funcion que renderiza el html del inicio
         const renderizarIndex = (req, res) => {
            console.log(__dirname);
            res.sendFile(path.resolve(__dirname, 'front/index.html'));
        }   
    
    //funcion que renderiza el html de la campana
        const renderizarCampana = (req, res) => {
            console.log(__dirname);
            res.sendFile(path.resolve(__dirname, 'front/index_camp.html'));
        }

    //funcion que renderiza el html del agente
        const renderizarAgente = (req, res) => {
            console.log(__dirname);
            res.sendFile(path.resolve(__dirname, 'front/agente.html'));
        }

    //funcion que renderiza el html de la lider de campana
        const renderizarLc = (req, res) => {
            console.log(__dirname);
            res.sendFile(path.resolve(__dirname, 'front/lider_campana.html'));
        }

    //funcion que renderiza el html del adminisrador
        const renderizarAdmin = (req, res) => {
            console.log(__dirname);
            res.sendFile(path.resolve(__dirname, 'front/p_admin/admin.html'));
        }



//por ultimo exportamos los metodos que hayamos creado
module.exports = {
    pool,
    getUsers,
    getUser,
    getCampanas,
    getDonaciones,
    getProductos,
    setUserAdmin,
    setUserAgente,
    setUserLc,
    setCampana,
    setDonacion,
    setOrden,
    setProducto,
    deleteUserAdmin,
    deleteUserAgente,
    deleteUserLc,
    deleteCampana,
    deleteProducto,
    renderizarEntrada,
    renderizarAgente,
    renderizarCampana,
    renderizarIndex,
    renderizarLc,
    renderizarAdmin
}