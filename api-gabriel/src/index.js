//linea generada por express al exportar su funcion urlencoded en las lineas 12-13
const { urlencoded } = require('body-parser');

const express = require('express');
const app = express();

//MIDELWARES

    //esto permite que express entendar el codigo JS que le envien
    app.use(express.json());

    //esto permite que express pueda recibir formularios en la url
    app.use(express.urlencoded({extended: false}));

//

//RUTAS

    //esto importa las rutas permitiendo llamarlas cuando se ejecute la app
    app.use(require('./routes/index.routes.js'));

//

app.listen(3000, (req, res) => {
    console.log('SERVER ON PORT 3000');
})