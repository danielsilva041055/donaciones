// aqui se se describen las rutas del servidor y que funciones invocan

//se importa la funcion Router de express
    const { Router } = require('express');
    const router = Router();


//importamos las funciones qeu definimos en Controllers
    const { 
        getUsers,
        getUser,
        getCampanas,
        getDonaciones,
        getProductos,
        setUserAdmin,
        setUserAgente,
        setUserLc,
        setProducto,
        deleteUserAdmin,
        deleteUserAgente,
        deleteUserLc,
        deleteCampana,
        deleteProducto,
        setCampana,
        setDonacion,
        setOrden,
        renderizarEntrada,
        renderizarAgente,
        renderizarCampana,
        renderizarIndex,
        renderizarLc,
        renderizarAdmin
    } = require('../controllers/index.controller.js')

//asignamos su funcion a su ruta correspondiente
    router.get('/', renderizarEntrada)
    router.get('/admin/users', getUsers);
    router.post('/admin/setAdmin', setUserAdmin);
    router.post('/admin/setAgente', setUserAgente);
    router.post('/admin/setLc', setUserLc);
    router.delete('/admin/deleteAdmin/:nombre', deleteUserAdmin);
    router.delete('/admin/deleteAgente/:nombre', deleteUserAgente);
    router.delete('/admin/deleteLc/:nombre', deleteUserLc);
    router.delete('/lc/deleteProducto/:id', deleteProducto);
    router.delete('/admin/deleteCampana/:id', deleteCampana);
    router.get('/login/:nombre/:pass', getUser);
    router.get('/agente', renderizarAgente);
    router.get('/camp', renderizarCampana);
    router.get('/inicio', renderizarIndex);
    router.get('/lc', renderizarLc);
    router.get('/admin', renderizarAdmin);
    router.get('/admin/campanas', getCampanas);
    router.post('/admin/setCampana', setCampana);
    router.post('/donacion/setDonacion', setDonacion);
    router.post('/donacion/setOrden', setOrden);
    router.get('/user/donaciones', getDonaciones);
    router.post('/lc/setProducto', setProducto);
    router.get('/lc/productos', getProductos);

//por ultimo exportamos las rutas asi
module.exports = router;