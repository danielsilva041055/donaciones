
-----------------------------------------------------------------------------------------------------

-- Functions

CREATE FUNCTION public.actualizar_montos() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
	UPDATE public.campana
		SET total_rec = total_rec + NEW.monto
		WHERE id = NEW.id;
	
	RETURN NEW;
END
$$;


ALTER FUNCTION public.actualizar_montos() OWNER TO postgres;

--
-- TOC entry 218 (class 1255 OID 74223)
-- Name: actualizar_montos2(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.actualizar_montos2() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  	UPDATE public.campana
  		SET total_rec = total_rec - OLD.monto
  		WHERE id = OLD.id;
	
  	RETURN OLD;
  END
$$;


ALTER FUNCTION public.actualizar_montos2() OWNER TO postgres;

-----------------------------------------------------------------------------------------------------

--
-- TOC entry 203 (class 1259 OID 65938)
-- Name: asociar_campaña; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."asociar_campaña" (
    id serial NOT NULL,
    "id_campaña" integer,
    id_agente integer,
    rec_ind double precision,
    total_rec double precision,
    "ROL" integer
);


ALTER TABLE public."asociar_campaña" OWNER TO postgres;

-----------------------------------------------------------------------------------------------------

--
-- TOC entry 205 (class 1259 OID 65943)
-- Name: campana; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.campana (
    id serial NOT NULL,
    monto_obj double precision,
    total_rec double precision DEFAULT 0 NOT NULL,
    nombre character varying,
    descripcion character varying
);


ALTER TABLE public.campana OWNER TO postgres;

-----------------------------------------------------------------------------------------------------

--
-- TOC entry 207 (class 1259 OID 65948)
-- Name: donacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.donacion (
    id serial NOT NULL,
    id_camp integer,
    monto double precision,
    status smallint
);


ALTER TABLE public.donacion OWNER TO postgres;

-----------------------------------------------------------------------------------------------------

--
-- TOC entry 209 (class 1259 OID 65953)
-- Name: orden; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.orden (
    id serial NOT NULL,
    id_donacion integer,
    id_product integer,
    cantidad integer,
    monto double precision
);


ALTER TABLE public.orden OWNER TO postgres;

-----------------------------------------------------------------------------------------------------

--
-- TOC entry 211 (class 1259 OID 65958)
-- Name: posee; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.posee (
    id serial NOT NULL,
    id_donacion integer,
    id_agente integer
);


ALTER TABLE public.posee OWNER TO postgres;

-----------------------------------------------------------------------------------------------------

--
-- TOC entry 213 (class 1259 OID 65963)
-- Name: productos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.productos (
    id_camp integer,
    precio double precision,
    nombre character varying NOT NULL,
    stock integer,
    id serial NOT NULL
);


ALTER TABLE public.productos OWNER TO postgres;

-----------------------------------------------------------------------------------------------------

--
-- TOC entry 215 (class 1259 OID 65971)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario (
    id serial NOT NULL,
    "contraseña" character varying NOT NULL,
    nombre_usuario character varying NOT NULL,
    "ISadmin" boolean,
    nombre character varying,
    visitas integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.usuario OWNER TO postgres;

-----------------------------------------------------------------------------------------------------

--
-- TOC entry 2738 (class 2606 OID 65987)
-- Name: asociar_campaña asociar_campaña_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."asociar_campaña"
    ADD CONSTRAINT "asociar_campaña_pkey" PRIMARY KEY (id);


--
-- TOC entry 2742 (class 2606 OID 65989)
-- Name: campana campana_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.campana
    ADD CONSTRAINT campana_pkey PRIMARY KEY (id);


--
-- TOC entry 2744 (class 2606 OID 65991)
-- Name: donacion donacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.donacion
    ADD CONSTRAINT donacion_pkey PRIMARY KEY (id);


--
-- TOC entry 2746 (class 2606 OID 65993)
-- Name: productos productos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.productos
    ADD CONSTRAINT productos_pkey PRIMARY KEY (id);


--
-- TOC entry 2748 (class 2606 OID 65995)
-- Name: usuario usuario_correo_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_correo_key UNIQUE (correo);


--
-- TOC entry 2750 (class 2606 OID 65997)
-- Name: usuario usuario_mombre_usuario_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_mombre_usuario_key UNIQUE (nombre_usuario);


--
-- TOC entry 2752 (class 2606 OID 65999)
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);


--
-- TOC entry 2739 (class 1259 OID 66000)
-- Name: fki_fk_agente; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_fk_agente ON public."asociar_campaña" USING btree (id_agente);


--
-- TOC entry 2740 (class 1259 OID 66001)
-- Name: fki_fk_campaña; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "fki_fk_campaña" ON public."asociar_campaña" USING btree ("id_campaña");


--
-- TOC entry 2761 (class 2620 OID 74225)
-- Name: donacion restar_donacion; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER restar_donacion AFTER DELETE ON public.donacion FOR EACH ROW EXECUTE FUNCTION public.actualizar_montos2();


--
-- TOC entry 2763 (class 2620 OID 74227)
-- Name: orden restar_orden; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER restar_orden AFTER DELETE ON public.orden FOR EACH ROW EXECUTE FUNCTION public.actualizar_montos2();


--
-- TOC entry 2762 (class 2620 OID 74224)
-- Name: donacion sumar_donacion; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER sumar_donacion AFTER INSERT ON public.donacion FOR EACH ROW EXECUTE FUNCTION public.actualizar_montos();


--
-- TOC entry 2764 (class 2620 OID 74226)
-- Name: orden sumar_orden; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER sumar_orden AFTER INSERT ON public.orden FOR EACH ROW EXECUTE FUNCTION public.actualizar_montos();


--
-- TOC entry 2753 (class 2606 OID 66002)
-- Name: asociar_campaña fk_agente; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."asociar_campaña"
    ADD CONSTRAINT fk_agente FOREIGN KEY (id_agente) REFERENCES public.usuario(id);


--
-- TOC entry 2758 (class 2606 OID 66007)
-- Name: posee fk_agente; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.posee
    ADD CONSTRAINT fk_agente FOREIGN KEY (id_agente) REFERENCES public.usuario(id);


--
-- TOC entry 2754 (class 2606 OID 66012)
-- Name: asociar_campaña fk_campaña; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."asociar_campaña"
    ADD CONSTRAINT "fk_campaña" FOREIGN KEY ("id_campaña") REFERENCES public.campana(id);


--
-- TOC entry 2925 (class 0 OID 0)
-- Dependencies: 2754
-- Name: CONSTRAINT "fk_campaña" ON "asociar_campaña"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT "fk_campaña" ON public."asociar_campaña" IS 'sas';


--
-- TOC entry 2760 (class 2606 OID 66017)
-- Name: productos fk_campaña; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.productos
    ADD CONSTRAINT "fk_campaña" FOREIGN KEY (id_camp) REFERENCES public.campana(id);


--
-- TOC entry 2755 (class 2606 OID 66022)
-- Name: donacion fk_campaña; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.donacion
    ADD CONSTRAINT "fk_campaña" FOREIGN KEY (id_camp) REFERENCES public.campana(id);


--
-- TOC entry 2759 (class 2606 OID 66027)
-- Name: posee fk_donacion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.posee
    ADD CONSTRAINT fk_donacion FOREIGN KEY (id_donacion) REFERENCES public.donacion(id);


--
-- TOC entry 2756 (class 2606 OID 66032)
-- Name: orden fk_donacion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orden
    ADD CONSTRAINT fk_donacion FOREIGN KEY (id_donacion) REFERENCES public.donacion(id);


--
-- TOC entry 2757 (class 2606 OID 66037)
-- Name: orden fk_producto; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orden
    ADD CONSTRAINT fk_producto FOREIGN KEY (id_product) REFERENCES public.productos(id);


