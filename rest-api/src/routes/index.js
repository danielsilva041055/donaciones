const { Router } = require('express');
const router = Router();
const path = require('path');

const { getAdmin, createAdmin, getAdminbyid, deleteAdmin, Adminupdate } = require('../controllers/index.controller');
const { getAgente, createAgente, getAgentebyid, deleteAgente, Agenteupdate } = require('../controllers/index.controller');
const { getLider, createLider, getLidbyid, deleteLider, Liderupdate } = require('../controllers/index.controller');
const { getCampana, createCamp, getCampbyid, deleteCamp, Campupdate } = require('../controllers/index.controller');
const { getDonacion, createDona, getDonacionbyid, deleteDona, Donaupdate } = require('../controllers/index.controller');
const { getAgentab, createAgentab, getAgentabyid, deleteAgentab, Agentabupdate } = require('../controllers/index.controller');
const { getLidertab, createLidertab, getLidertabyid, deleteLidertab, Lidertabupdate } = require('../controllers/index.controller');
const { getOrden, createOrden, getOrdenbyid, deleteOrden, Ordenupdate } = require('../controllers/index.controller');
const { getProd, createProd, getProdbyid, deleteProd, Produpdate } = require('../controllers/index.controller');
const { getUser, createUser, getUserbyid, deleteUser, Userupdate } = require('../controllers/index.controller');



// En las siguientes adiciones enlace los html de cada pestaña a la ruta correspondiente, 
// al leer este codigo posiblemente surjan preguntas. Algunos metodos "GetAlgo" estan enlazados con algunas rutas en las
//que los nombres no corresponden, ejem '/camp', getCamp es una ruta que tiene una correspondencia de nombre.
// y hay otras como: '/entrada', getDona no lo tienen.
//la razon de esta modificacion inconclusa era probar las pestañas de nuestro servidor basandome en los html que hizo Gabriel
// ya los metodos getDona, getPago y demás serán MODIFICADOS conforme vayamos trabajando y tengamos los metodos que vamos a usar para hacer consultas al servidor y a la base de datos

router.get('/log', function(req, res){
    res.sendFile(path.join('C:/Users/Daniel/Desktop/rest-api/src/front/login/login.html')); //ruta del html de lider de campaña
});

/*router.get('/camp', function(req, res){
    res.sendFile(path.join('C:/Users/Daniel/Desktop/rest-api/src/front/p_camp/index_camp.html')); //ruta del html de lider de campaña
});*/
router.get('/camp', getCampana);
router.get('/camp/:id', getCampbyid);
router.post('/camp', createCamp);
router.post('/camp/:id', deleteCamp);
router.post('/camp/:id', Campupdate);

//PARA VISUALIZAR EL HTML EN LA RUTA ELIMINAR LOS COMENTARIOS
/*router.get('/admin', function(req, res){
    res.sendFile(path.join('C:/Users/Daniel/Desktop/rest-api/src/front/p_admin/admin.html')); //ruta del html del Admin
});*/
router.get('/admin', getAdmin);  //posiblemente modificar
router.get('/admin/:contrasena', getAdminbyid); //clave p
router.post('/admin', createAdmin);
router.post('/admin/:id', deleteAdmin);
router.post('/admin/:id', Adminupdate);

/*router.get('/agent', function(req, res){
    res.sendFile(path.join('C:/Users/Daniel/Desktop/rest-api/src/front/p_agente/agente.html')); //ruta del html del Agente
});*/
router.get('/agente', getAgente);  //posiblemente modificar
router.get('/agente/:contrasena', getAgentebyid); //clave p
router.post('/agente', createAgente);
router.post('/agente/:id', deleteAgente);
router.post('/agente/:id', Agenteupdate);

/*router.get('/lider', function(req, res){
    res.sendFile(path.join('C:/Users/Daniel/Desktop/rest-api/src/front/p_lider_campana/lider_campana.html')); //ruta del html del Agente
});*/
router.get('/lider', getLider);  //posiblemente modificar
router.get('/lider/:contrasena', getLidbyid); //clave p
router.post('/lider', createLider);
router.post('/lider/:id', deleteLider);
router.post('/lider/:id', Liderupdate);

router.get('/entrada', function(req, res){
    res.sendFile(path.join('C:/Users/Daniel/Desktop/rest-api/src/front/p_entrada/entrada.html')); //ruta del html de lider de campaña
});
router.get('/entrada', );  //modificar
router.post('/entrada', createDona);

router.get('/home', function(req, res){
    
    res.sendFile(path.join('C:/Users/Daniel/Desktop/rest-api/src/front/p_index/index.html')); //ruta del html de lider de campaña
    
});
router.get('/home', ); //modificar
router.post('/home', );

router.get('/dona', getDonacion);  //posiblemente modificar
router.get('/dona/:id', getDonacionbyid); //clave p
router.post('/dona', createDona);
router.post('/dona/:id', deleteDona);
router.post('/dona/:id', Donaupdate);

router.get('/agentab', getAgentab);  //posiblemente modificar
router.get('/agentab/:id', getAgentabyid); //clave p
router.post('/agentab', createAgentab);
router.post('/agentab/:id', deleteAgentab);
router.post('/agentab/:id', Agentabupdate);

router.get('/lidertab', getLidertab);  //posiblemente modificar
router.get('/lidertab/:id', getLidertabyid); //clave p
router.post('/lidertab', createLidertab);
router.post('/lidertab/:id', deleteLidertab);
router.post('/lidertab/:id', Lidertabupdate);

router.get('/orden', getOrden);  //posiblemente modificar
router.get('/orden/:id', getOrdenbyid); //clave p
router.post('/orden', createOrden);
router.post('/orden/:id', deleteOrden);
router.post('/orden/:id', Ordenupdate);

router.get('/productos', getProd);
router.get('/orden/:id', getProdbyid); //clave p
router.post('/productos', createProd);
router.post('/orden/:id', deleteProd);
router.post('/orden/:id', Produpdate);

router.get('/usuario', getUser);
router.get('/usuario/:id', getUserbyid); //clave p
router.post('/usuario', createUser);
router.post('/usuario/:id', deleteUser);
router.post('/usuario/:id', Userupdate);

//----------------------------------------------------// MODI





module.exports = router;
