const express = require('express');
const app = express();
const path = require('path');

// middlewares
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

// routes
app.use(require('./routes/index'));
/*app.get('/', function(req, res){
    res.sendFile(path.join('./front/p_lider_campana/lider_campana.html'));
});*/

//htlms
//app.use(require('./front/lider_camp'));

app.listen(3000);
console.log('Server on port 3000');
