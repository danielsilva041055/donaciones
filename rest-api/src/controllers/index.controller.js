const { Pool } = require('pg');

const pool = new Pool({
    host: 'localhost' ,
    user: 'postgres' ,
    password: '1234' ,  //contraseña que crearin en la instalacion de su postgres
    database: 'gabriel' ,  // nombre por el que tengan la bd de Gabriel
    port: '5432'
});

const getAdmin = async (req, res) => {
    const response = await pool.query('SELECT * FROM administrador');
    res.status(200).json(response.rows);
}

const createAdmin = async (req, res) => {
    const { name, meta } = req.body;
    const response = await pool.query('INSERT INTO administrador (name, meta) VALUES ($1, &2)', [name, meta]); //ingresar los datos que correspondan
    console.log(response);
    res.jason({
        message: 'Administrator added Succesfully',
        body: {
            uder: {name, meta}
        }
    })
    res.send('Admin created');
};

const getAdminbyid = async (req, res) => {
    const pass = req.params.contrasena;
    const response = await pool.query('SELECT * FROM administrador WHERE contrasena = $1', [pass]);
    res.json(response.rows);
};

const deleteAdmin = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('DELETE FROM administrador WHERE id = $1', [id]);
    console.log(response);
    res.json('admin ${id} deleted successfully'); // ${id} enumera el objeto eliminado (id, si corresponde)
};

const Adminupdate = async (req, res) => {
    const id = req.params.id;
    const { name, meta } = req.body;
    const response = await pool.query('UPDATE administrador SET name = $1, meta = $2 WHERE ide = $3'[
        name,
        meta,
        id
    ]);
    console.log(response);
    res.json('admin Updated successfully');
};

//metodos api
//-------------------------------------------------------------------------------------

const getAgente = async (req, res) => {
    const response = await pool.query('SELECT * FROM agente');
    res.status(200).json(response.rows);
}

const createAgente = async (req, res) => {
    const { name, meta } = req.body;
    const response = await pool.query('INSERT INTO agente (name, meta) VALUES ($1, &2)', [name, meta]); //ingresar los datos que correspondan
    console.log(response);
    res.jason({
        message: 'agente added Succesfully',
        body: {
            uder: {name, meta}
        }
    })
    res.send('Admin created');
};

const getAgentebyid = async (req, res) => {
    const pass = req.params.contrasena;
    const response = await pool.query('SELECT * FROM agente WHERE contrasena = $1', [pass]);  //guia para corregir los demas modulos
    res.json(response.rows);
};

const deleteAgente = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('DELETE FROM agente WHERE id = $1', [id]);
    console.log(response);
    res.json('agente ${id} deleted successfully'); // ${id} enumera el objeto eliminado (id, si corresponde)
};

const Agenteupdate = async (req, res) => {
    const id = req.params.id;
    const { name, meta } = req.body;
    const response = await pool.query('UPDATE agente SET name = $1, meta = $2 WHERE ide = $3'[
        name,
        meta,
        id
    ]);
    console.log(response);
    res.json('agent Updated successfully');
};

//-----------------------------------------------------------------------------------------------------------

const getCamp = async (req, res) => {
    const response = await pool.query('SELECT * FROM campaign');
    res.status(200).json(response.rows);
}

//------------------------------------------------------------------------------------------

const getLider = async (req, res) => {
    const response = await pool.query('SELECT * FROM lider_campana');
    res.status(200).json(response.rows);
}

const createLider = async (req, res) => {
    const { name, meta } = req.body;
    const response = await pool.query('INSERT INTO lider_campana (name, meta) VALUES ($1, &2)', [name, meta]); //ingresar los datos que correspondan
    console.log(response);
    res.jason({
        message: 'lider added Succesfully',
        body: {
            uder: {name, meta}
        }
    })
    res.send('Admin created');
};

const getLiderbyid = async (req, res) => {
    const pass = req.params.contrasena;
    const response = await pool.query('SELECT * FROM lider_campana WHERE contrasena = $1', [pass]);  //guia para corregir los demas modulos
    res.json(response.rows);
};

const getLidbyid = async (req, res) => {
    const pass = req.params.contrasena;
    const response = await pool.query('SELECT * FROM lider_campana WHERE contrasena = $1', [pass]);  //guia para corregir los demas modulos
    res.json(response.rows);
};

const deleteLider = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('DELETE FROM lider_campana WHERE id = $1', [id]);
    console.log(response);
    res.json('lider ${id} deleted successfully'); // ${id} enumera el objeto eliminado (id, si corresponde)
};

const Liderupdate = async (req, res) => {
    const id = req.params.id;
    const { name, meta } = req.body;
    const response = await pool.query('UPDATE lider_campana SET name = $1, meta = $2 WHERE ide = $3'[
        name,
        meta,
        id
    ]);
    console.log(response);
    res.json('lider Updated successfully');
};

//-----------------------------------------------------------------------------------------------------------

const getCampana = async (req, res) => {
    const response = await pool.query('SELECT * FROM campana');
    res.status(200).json(response.rows);
}

const createCamp = async (req, res) => {
    const { name, meta } = req.body;
    const response = await pool.query('INSERT INTO campana (name, meta) VALUES ($1, &2)', [name, meta]);
    console.log(response);
    res.jason({
        message: 'Campaign added Succesfully',
        body: {
            uder: {name, meta}
        }
    })
    res.send('Campaign created');
};

const getCampbyid = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('SELECT * FROM campana WHERE id = $1', [id]);
    res.json(response.rows);
};

const deleteCamp = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('DELETE FROM campana WHERE id = $1', [id]);
    console.log(response);
    res.json('Campign ${id} deleted successfully');
};

const Campupdate = async (req, res) => {
    const id = req.params.id;
    const { name, meta } = req.body;
    const response = await pool.query('UPDATE campana SET name = $1, meta = $2 WHERE ide = $3'[
        name,
        meta,
        id
    ]);
    console.log(response);
    res.json('Campaign Updated successfully');
};

//-------------------------------------------------------------------------------------------------

const getDonacion = async (req, res) => {
    const response = await pool.query('SELECT * FROM donacion');
    res.status(200).json(response.rows);
}

const createDona = async (req, res) => {
    const { name, meta } = req.body;
    const response = await pool.query('INSERT INTO donacion (name, meta) VALUES ($1, &2)', [name, meta]);
    console.log(response);
    res.jason({
        message: 'Campaign added Succesfully',
        body: {
            uder: {name, meta}
        }
    })
    res.send('donacion created');
};

const getDonacionbyid = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('SELECT * FROM donacion WHERE id = $1', [id]);
    res.json(response.rows);
};

const deleteDona = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('DELETE FROM donacion WHERE id = $1', [id]);
    console.log(response);
    res.json('donacion ${id} deleted successfully');
};

const Donaupdate = async (req, res) => {
    const id = req.params.id;
    const { name, meta } = req.body;
    const response = await pool.query('UPDATE donacion SET name = $1, meta = $2 WHERE ide = $3'[
        name,
        meta,
        id
    ]);
    console.log(response);
    res.json('donacion Updated successfully');
};

//-------------------------------------------------------------------------------------

const getAgentab = async (req, res) => {
    const response = await pool.query('SELECT * FROM asocia_agente_dashboard');
    res.status(200).json(response.rows);
}

const createAgentab = async (req, res) => {
    const { name, meta } = req.body;
    const response = await pool.query('INSERT INTO asocia_agente_dashboard (name, meta) VALUES ($1, &2)', [name, meta]);
    console.log(response);
    res.jason({
        message: 'agente tab added Succesfully',
        body: {
            uder: {name, meta}
        }
    })
    res.send('donacion created');
};

const getAgentabyid = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('SELECT * FROM asocia_agente_dashboard WHERE id = $1', [id]);
    res.json(response.rows);
};

const deleteAgentab = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('DELETE FROM asocia_agente_dashboard WHERE id = $1', [id]);
    console.log(response);
    res.json('agente tab ${id} deleted successfully');
};

const Agentabupdate = async (req, res) => {
    const id = req.params.id;
    const { name, meta } = req.body;
    const response = await pool.query('UPDATE asocia_agente_dashboard SET name = $1, meta = $2 WHERE ide = $3'[
        name,
        meta,
        id
    ]);
    console.log(response);
    res.json('agente tab Updated successfully');
};

//------------------------------------------------------------------------------------

const getLidertab = async (req, res) => {
    const response = await pool.query('SELECT * FROM asocia_lider_dashboard');
    res.status(200).json(response.rows);
}

const createLidertab = async (req, res) => {
    const { name, meta } = req.body;
    const response = await pool.query('INSERT INTO asocia_lider_dashboard (name, meta) VALUES ($1, &2)', [name, meta]);
    console.log(response);
    res.jason({
        message: 'lider tab added Succesfully',
        body: {
            uder: {name, meta}
        }
    })
    res.send('lider tab created');
};

const getLidertabyid = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('SELECT * FROM asocia_lider_dashboard WHERE id = $1', [id]);
    res.json(response.rows);
};

const deleteLidertab = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('DELETE FROM asocia_lider_dashboard WHERE id = $1', [id]);
    console.log(response);
    res.json('lider tab ${id} deleted successfully');
};

const Lidertabupdate = async (req, res) => {
    const id = req.params.id;
    const { name, meta } = req.body;
    const response = await pool.query('UPDATE asocia_lider_dashboard SET name = $1, meta = $2 WHERE ide = $3'[
        name,
        meta,
        id
    ]);
    console.log(response);
    res.json('lider tab Updated successfully');
};

//-----------------------------------------------------------------------------------------------------------

const getOrden = async (req, res) => {
    const response = await pool.query('SELECT * FROM orden');
    res.status(200).json(response.rows);
}

const createOrden = async (req, res) => {
    const { name, meta } = req.body;
    const response = await pool.query('INSERT INTO orden (name, meta) VALUES ($1, &2)', [name, meta]);
    console.log(response);
    res.jason({
        message: 'orden added Succesfully',
        body: {
            uder: {name, meta}
        }
    })
    res.send('orden created');
};

const getOrdenbyid = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('SELECT * FROM orden WHERE id = $1', [id]);
    res.json(response.rows);
};

const deleteOrden = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('DELETE FROM orden WHERE id = $1', [id]);
    console.log(response);
    res.json('orden ${id} deleted successfully');
};

const Ordenupdate = async (req, res) => {
    const id = req.params.id;
    const { name, meta } = req.body;
    const response = await pool.query('UPDATE orden SET name = $1, meta = $2 WHERE ide = $3'[
        name,
        meta,
        id
    ]);
    console.log(response);
    res.json('orden Updated successfully');
};

//-------------------------------------------------------------------------------------

const getProd = async (req, res) => {
    const response = await pool.query('SELECT * FROM producto');
    res.status(200).json(response.rows);
}

const createProd = async (req, res) => {
    const { name, meta } = req.body;
    const response = await pool.query('INSERT INTO producto (name, meta) VALUES ($1, &2)', [name, meta]);
    console.log(response);
    res.jason({
        message: 'producto added Succesfully',
        body: {
            uder: {name, meta}
        }
    })
    res.send('producto created');
};

const getProdbyid = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('SELECT * FROM producto WHERE id = $1', [id]);
    res.json(response.rows);
};

const deleteProd = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('DELETE FROM producto WHERE id = $1', [id]);
    console.log(response);
    res.json('producto ${id} deleted successfully');
};

const Produpdate = async (req, res) => {
    const id = req.params.id;
    const { name, meta } = req.body;
    const response = await pool.query('UPDATE producto SET name = $1, meta = $2 WHERE ide = $3'[
        name,
        meta,
        id
    ]);
    console.log(response);
    res.json('producto Updated successfully');
};

//---------------------------------------------------------------------------------------------

const getUser = async (req, res) => {
    const response = await pool.query('SELECT * FROM usuario');
    res.status(200).json(response.rows);
}

const createUser = async (req, res) => {
    const { name, email } = req.body;
    const response = await pool.query('INSERT INTO usuario (name, email) VALUES ($1, &2)', [name, email]);
    console.log(response);
    res.jason({
        message: 'User added Succesfully',
        body: {
            uder: {name, email}
        }
    })
    res.send('usuario created');
};

const getUserbyid = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('SELECT * FROM usuario WHERE id = $1', [id]);
    res.json(response.rows);
};

const deleteUser = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('DELETE FROM usuario WHERE id = $1', [id]);
    console.log(response);
    res.json('User ${id} deleted successfully');
};

const Userupdate = async (req, res) => {
    const id = req.params.id;
    const { name, email } = req.body;
    const response = await pool.query('UPDATE usuario SET name = $1, email = $2 WHERE ide = $3'[
        name,
        email,
        id
    ]);
    console.log(response);
    res.json('User Updated successfully');
};

//-------------------------------------------------------------------------------------------------



module.exports = {
    getAdmin,
    getAdminbyid,
    deleteAdmin,
    Adminupdate,
    createAdmin,
    getAgente,
    getAgentebyid,
    deleteAgente,
    Agenteupdate,
    createAgente,
    getLider,
    getLidbyid,
    deleteLider,
    Liderupdate,
    createLider,
    getCampbyid,
    deleteCamp,
    Campupdate,
    getCampana,
    createCamp,
    getDonacion,
    createDona,
    getDonacionbyid,
    deleteDona,
    Donaupdate,
    createAgentab,
    getAgentab,
    getAgentabyid,
    deleteAgentab,
    Agentabupdate,
    createLidertab,
    getLidertab,
    getLidertabyid,
    deleteLidertab,
    Lidertabupdate,
    getOrden,
    getOrdenbyid,
    deleteOrden,
    Ordenupdate,
    createOrden,
    getProd,
    createProd,
    getProdbyid,
    deleteProd,
    Produpdate,
    getUser,
    getUserbyid,
    deleteUser,
    Userupdate,
    createUser,
    
}

/*----------------------------------------------------------------------------------*/

