CREATE DATABASE donaciones;

CREATE TABLE campaign(
    id SERIAL PRIMARY KEY,
    name VARCHAR(40),
    meta DECIMAL(20,4),
    total DECIMAL(20,4)
);

INSERT INTO campaign (name, meta, total) VALUES
    ('Cancer', 30.50, 20),
    ('tu madre', 2346645.32, 300000);

INSERT INTO productos (precio, tipo) VALUES
    (30.50, 'prod'),
    (100.50, 'Serv');
    
    
