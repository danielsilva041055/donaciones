PGDMP          '                y            gabriel    10.16    10.16 C    R           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            S           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            T           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            U           1262    24576    gabriel    DATABASE     �   CREATE DATABASE gabriel WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
    DROP DATABASE gabriel;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            V           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            W           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1255    24719    actualizar_montos()    FUNCTION     �   CREATE FUNCTION public.actualizar_montos() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
	UPDATE public.campana
		SET total_recaudado = total_recaudado + NEW.monto
		WHERE id = NEW.id_campana;
	
	RETURN NEW;
END
$$;
 *   DROP FUNCTION public.actualizar_montos();
       public       postgres    false    3    1            �            1255    24720    actualizar_montos2()    FUNCTION     �   CREATE FUNCTION public.actualizar_montos2() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  	UPDATE public.campana
  		SET total_recaudado = total_recaudado - OLD.monto
  		WHERE id = OLD.id_campana;
	
  	RETURN OLD;
  END
$$;
 +   DROP FUNCTION public.actualizar_montos2();
       public       postgres    false    1    3            �            1259    24577    usuario    TABLE     `   CREATE TABLE public.usuario (
    contrasena character varying,
    correo character varying
);
    DROP TABLE public.usuario;
       public         postgres    false    3            �            1259    24601    administrador    TABLE     �   CREATE TABLE public.administrador (
    nombre character varying NOT NULL,
    nivel integer DEFAULT 3
)
INHERITS (public.usuario);
 !   DROP TABLE public.administrador;
       public         postgres    false    3    196            �            1259    24583    agente    TABLE     }   CREATE TABLE public.agente (
    nombre character varying NOT NULL,
    nivel integer DEFAULT 1
)
INHERITS (public.usuario);
    DROP TABLE public.agente;
       public         postgres    false    196    3            �            1259    24687    asocia_agente_dashboard    TABLE     �   CREATE TABLE public.asocia_agente_dashboard (
    id_campana integer NOT NULL,
    nombre_agente character varying NOT NULL,
    recaudado_indv numeric,
    recaudado_total numeric,
    porcentaje_visitas numeric
);
 +   DROP TABLE public.asocia_agente_dashboard;
       public         postgres    false    3            �            1259    24703    asocia_lider_dashboard    TABLE     �   CREATE TABLE public.asocia_lider_dashboard (
    id_campana integer,
    nombre_lider character varying,
    recaudado_indv integer,
    recaudado_total integer,
    porcenaje_visitas numeric,
    porcentaje_donantes numeric
);
 *   DROP TABLE public.asocia_lider_dashboard;
       public         postgres    false    3            �            1259    24612    campana    TABLE     �   CREATE TABLE public.campana (
    id integer NOT NULL,
    monto_objetivo integer,
    total_recaudado integer,
    porcentaje_visitas integer,
    nombre character varying
);
    DROP TABLE public.campana;
       public         postgres    false    3            �            1259    24610    campana_id_seq    SEQUENCE     �   CREATE SEQUENCE public.campana_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.campana_id_seq;
       public       postgres    false    3    201            X           0    0    campana_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.campana_id_seq OWNED BY public.campana.id;
            public       postgres    false    200            �            1259    24623    donacion    TABLE     e   CREATE TABLE public.donacion (
    id integer NOT NULL,
    id_campana integer,
    monto integer
);
    DROP TABLE public.donacion;
       public         postgres    false    3            �            1259    24621    donacion_id_seq    SEQUENCE     �   CREATE SEQUENCE public.donacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.donacion_id_seq;
       public       postgres    false    3    203            Y           0    0    donacion_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.donacion_id_seq OWNED BY public.donacion.id;
            public       postgres    false    202            �            1259    24592    lider_campana    TABLE     !  CREATE TABLE public.lider_campana (
    nombre character varying NOT NULL,
    nivel integer DEFAULT 2
)
INHERITS (public.usuario);
ALTER TABLE ONLY public.lider_campana ALTER COLUMN contrasena SET NOT NULL;
ALTER TABLE ONLY public.lider_campana ALTER COLUMN contrasena SET STORAGE PLAIN;
 !   DROP TABLE public.lider_campana;
       public         postgres    false    3    196            �            1259    24650    orden    TABLE     R   CREATE TABLE public.orden (
    id_producto integer
)
INHERITS (public.donacion);
    DROP TABLE public.orden;
       public         postgres    false    3    203            �            1259    24666    posee    TABLE     n   CREATE TABLE public.posee (
    id_donacion integer NOT NULL,
    nombre_agente character varying NOT NULL
);
    DROP TABLE public.posee;
       public         postgres    false    3            �            1259    24636    producto    TABLE     �   CREATE TABLE public.producto (
    id integer NOT NULL,
    nombre character varying,
    precio character varying,
    id_campana integer
);
    DROP TABLE public.producto;
       public         postgres    false    3            �            1259    24634    producto_id_seq    SEQUENCE     �   CREATE SEQUENCE public.producto_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.producto_id_seq;
       public       postgres    false    205    3            Z           0    0    producto_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.producto_id_seq OWNED BY public.producto.id;
            public       postgres    false    204            �
           2604    24615 
   campana id    DEFAULT     h   ALTER TABLE ONLY public.campana ALTER COLUMN id SET DEFAULT nextval('public.campana_id_seq'::regclass);
 9   ALTER TABLE public.campana ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    201    200    201            �
           2604    24626    donacion id    DEFAULT     j   ALTER TABLE ONLY public.donacion ALTER COLUMN id SET DEFAULT nextval('public.donacion_id_seq'::regclass);
 :   ALTER TABLE public.donacion ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    202    203    203            �
           2604    24653    orden id    DEFAULT     g   ALTER TABLE ONLY public.orden ALTER COLUMN id SET DEFAULT nextval('public.donacion_id_seq'::regclass);
 7   ALTER TABLE public.orden ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    206    202            �
           2604    24639    producto id    DEFAULT     j   ALTER TABLE ONLY public.producto ALTER COLUMN id SET DEFAULT nextval('public.producto_id_seq'::regclass);
 :   ALTER TABLE public.producto ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    205    204    205            E          0    24601    administrador 
   TABLE DATA               J   COPY public.administrador (contrasena, correo, nombre, nivel) FROM stdin;
    public       postgres    false    199   �N       C          0    24583    agente 
   TABLE DATA               C   COPY public.agente (contrasena, correo, nombre, nivel) FROM stdin;
    public       postgres    false    197   �N       N          0    24687    asocia_agente_dashboard 
   TABLE DATA               �   COPY public.asocia_agente_dashboard (id_campana, nombre_agente, recaudado_indv, recaudado_total, porcentaje_visitas) FROM stdin;
    public       postgres    false    208   O       O          0    24703    asocia_lider_dashboard 
   TABLE DATA               �   COPY public.asocia_lider_dashboard (id_campana, nombre_lider, recaudado_indv, recaudado_total, porcenaje_visitas, porcentaje_donantes) FROM stdin;
    public       postgres    false    209   (O       G          0    24612    campana 
   TABLE DATA               b   COPY public.campana (id, monto_objetivo, total_recaudado, porcentaje_visitas, nombre) FROM stdin;
    public       postgres    false    201   EO       I          0    24623    donacion 
   TABLE DATA               9   COPY public.donacion (id, id_campana, monto) FROM stdin;
    public       postgres    false    203   �O       D          0    24592    lider_campana 
   TABLE DATA               J   COPY public.lider_campana (contrasena, correo, nombre, nivel) FROM stdin;
    public       postgres    false    198   �O       L          0    24650    orden 
   TABLE DATA               C   COPY public.orden (id, id_campana, monto, id_producto) FROM stdin;
    public       postgres    false    206   P       M          0    24666    posee 
   TABLE DATA               ;   COPY public.posee (id_donacion, nombre_agente) FROM stdin;
    public       postgres    false    207   0P       K          0    24636    producto 
   TABLE DATA               B   COPY public.producto (id, nombre, precio, id_campana) FROM stdin;
    public       postgres    false    205   MP       B          0    24577    usuario 
   TABLE DATA               5   COPY public.usuario (contrasena, correo) FROM stdin;
    public       postgres    false    196   jP       [           0    0    campana_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.campana_id_seq', 2, true);
            public       postgres    false    200            \           0    0    donacion_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.donacion_id_seq', 1, false);
            public       postgres    false    202            ]           0    0    producto_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.producto_id_seq', 1, false);
            public       postgres    false    204            �
           2606    24609     administrador administrador_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.administrador
    ADD CONSTRAINT administrador_pkey PRIMARY KEY (nombre);
 J   ALTER TABLE ONLY public.administrador DROP CONSTRAINT administrador_pkey;
       public         postgres    false    199            �
           2606    24620    campana id-campana-pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.campana
    ADD CONSTRAINT "id-campana-pkey" PRIMARY KEY (id);
 C   ALTER TABLE ONLY public.campana DROP CONSTRAINT "id-campana-pkey";
       public         postgres    false    201            �
           2606    24628    donacion id-donacion-pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.donacion
    ADD CONSTRAINT "id-donacion-pkey" PRIMARY KEY (id);
 E   ALTER TABLE ONLY public.donacion DROP CONSTRAINT "id-donacion-pkey";
       public         postgres    false    203            �
           2606    24655    orden id-orden-pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.orden
    ADD CONSTRAINT "id-orden-pkey" PRIMARY KEY (id);
 ?   ALTER TABLE ONLY public.orden DROP CONSTRAINT "id-orden-pkey";
       public         postgres    false    206            �
           2606    24644    producto id-producto-pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.producto
    ADD CONSTRAINT "id-producto-pkey" PRIMARY KEY (id);
 E   ALTER TABLE ONLY public.producto DROP CONSTRAINT "id-producto-pkey";
       public         postgres    false    205            �
           2606    24600 !   lider_campana nombre-usuario-pkey 
   CONSTRAINT     e   ALTER TABLE ONLY public.lider_campana
    ADD CONSTRAINT "nombre-usuario-pkey" PRIMARY KEY (nombre);
 M   ALTER TABLE ONLY public.lider_campana DROP CONSTRAINT "nombre-usuario-pkey";
       public         postgres    false    198            �
           2606    24591    agente usuario-nombre-pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.agente
    ADD CONSTRAINT "usuario-nombre-pkey" PRIMARY KEY (nombre);
 F   ALTER TABLE ONLY public.agente DROP CONSTRAINT "usuario-nombre-pkey";
       public         postgres    false    197            �
           2620    24722    donacion restar_donacion    TRIGGER     {   CREATE TRIGGER restar_donacion AFTER DELETE ON public.donacion FOR EACH ROW EXECUTE PROCEDURE public.actualizar_montos2();
 1   DROP TRIGGER restar_donacion ON public.donacion;
       public       postgres    false    203    211            �
           2620    24724    orden restar_orden    TRIGGER     u   CREATE TRIGGER restar_orden AFTER DELETE ON public.orden FOR EACH ROW EXECUTE PROCEDURE public.actualizar_montos2();
 +   DROP TRIGGER restar_orden ON public.orden;
       public       postgres    false    206    211            �
           2620    24721    donacion sumar_donacion    TRIGGER     y   CREATE TRIGGER sumar_donacion AFTER INSERT ON public.donacion FOR EACH ROW EXECUTE PROCEDURE public.actualizar_montos();
 0   DROP TRIGGER sumar_donacion ON public.donacion;
       public       postgres    false    203    210            �
           2620    24723    orden sumar_orden    TRIGGER     s   CREATE TRIGGER sumar_orden AFTER INSERT ON public.orden FOR EACH ROW EXECUTE PROCEDURE public.actualizar_montos();
 *   DROP TRIGGER sumar_orden ON public.orden;
       public       postgres    false    210    206            �
           2606    24693 1   asocia_agente_dashboard agente-asocia_agente-fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.asocia_agente_dashboard
    ADD CONSTRAINT "agente-asocia_agente-fkey" FOREIGN KEY (nombre_agente) REFERENCES public.agente(nombre) ON UPDATE CASCADE ON DELETE CASCADE;
 ]   ALTER TABLE ONLY public.asocia_agente_dashboard DROP CONSTRAINT "agente-asocia_agente-fkey";
       public       postgres    false    208    197    2733            �
           2606    24672    posee agente-posee-fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.posee
    ADD CONSTRAINT "agente-posee-fkey" FOREIGN KEY (nombre_agente) REFERENCES public.agente(nombre) ON UPDATE CASCADE ON DELETE CASCADE;
 C   ALTER TABLE ONLY public.posee DROP CONSTRAINT "agente-posee-fkey";
       public       postgres    false    197    2733    207            �
           2606    24698 2   asocia_agente_dashboard campana-asocia_agente-fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.asocia_agente_dashboard
    ADD CONSTRAINT "campana-asocia_agente-fkey" FOREIGN KEY (id_campana) REFERENCES public.campana(id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.asocia_agente_dashboard DROP CONSTRAINT "campana-asocia_agente-fkey";
       public       postgres    false    201    208    2739            �
           2606    24709 0   asocia_lider_dashboard campana-asocia_lider-fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.asocia_lider_dashboard
    ADD CONSTRAINT "campana-asocia_lider-fkey" FOREIGN KEY (id_campana) REFERENCES public.campana(id) ON UPDATE CASCADE ON DELETE CASCADE;
 \   ALTER TABLE ONLY public.asocia_lider_dashboard DROP CONSTRAINT "campana-asocia_lider-fkey";
       public       postgres    false    2739    209    201            �
           2606    24629    donacion campana-donacion_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.donacion
    ADD CONSTRAINT "campana-donacion_fkey" FOREIGN KEY (id_campana) REFERENCES public.campana(id) ON UPDATE CASCADE ON DELETE CASCADE;
 J   ALTER TABLE ONLY public.donacion DROP CONSTRAINT "campana-donacion_fkey";
       public       postgres    false    2739    203    201            �
           2606    24656    orden campana-orden-fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.orden
    ADD CONSTRAINT "campana-orden-fkey" FOREIGN KEY (id_campana) REFERENCES public.campana(id) ON UPDATE CASCADE ON DELETE CASCADE;
 D   ALTER TABLE ONLY public.orden DROP CONSTRAINT "campana-orden-fkey";
       public       postgres    false    206    201    2739            �
           2606    24645    producto campana-producto-fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.producto
    ADD CONSTRAINT "campana-producto-fkey" FOREIGN KEY (id_campana) REFERENCES public.campana(id) ON UPDATE CASCADE ON DELETE CASCADE;
 J   ALTER TABLE ONLY public.producto DROP CONSTRAINT "campana-producto-fkey";
       public       postgres    false    2739    205    201            �
           2606    24677    posee donacion-posee-fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.posee
    ADD CONSTRAINT "donacion-posee-fkey" FOREIGN KEY (id_donacion) REFERENCES public.donacion(id) ON UPDATE CASCADE ON DELETE CASCADE;
 E   ALTER TABLE ONLY public.posee DROP CONSTRAINT "donacion-posee-fkey";
       public       postgres    false    203    207    2741            �
           2606    24714 .   asocia_lider_dashboard lider-asocia_lider-fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.asocia_lider_dashboard
    ADD CONSTRAINT "lider-asocia_lider-fkey" FOREIGN KEY (nombre_lider) REFERENCES public.lider_campana(nombre) ON UPDATE CASCADE ON DELETE CASCADE;
 Z   ALTER TABLE ONLY public.asocia_lider_dashboard DROP CONSTRAINT "lider-asocia_lider-fkey";
       public       postgres    false    198    209    2735            �
           2606    24682    posee orden-posee-fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.posee
    ADD CONSTRAINT "orden-posee-fkey" FOREIGN KEY (id_donacion) REFERENCES public.orden(id) ON UPDATE CASCADE ON DELETE CASCADE;
 B   ALTER TABLE ONLY public.posee DROP CONSTRAINT "orden-posee-fkey";
       public       postgres    false    207    206    2745            �
           2606    24661    orden producto-orden-fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.orden
    ADD CONSTRAINT "producto-orden-fkey" FOREIGN KEY (id_producto) REFERENCES public.producto(id) ON UPDATE CASCADE ON DELETE CASCADE;
 E   ALTER TABLE ONLY public.orden DROP CONSTRAINT "producto-orden-fkey";
       public       postgres    false    206    2743    205            E   5   x�34261�,H-�,�wH�M���HM)��4�ɘq&�%B����1z\\\ ���      C   6   x�34261�,�/�wH�M�����O,,���4�ɘq%��@���L�x� �[�      N      x������ � �      O      x������ � �      G   -   x�3�45�46�42�LN�KN-�2�46�X�sg�$r��qqq �\�      I      x������ � �      D   d   x�34261��*M�sH�M����29��@�f���řى0������D�$HΜ�,�,*b&恤@2���%�EPI0(���,�/��d�c����  (_      L      x������ � �      M      x������ � �      K      x������ � �      B      x������ � �     